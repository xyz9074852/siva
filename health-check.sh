#!/bin/bash
echo "container health check process going on"
echo "Healthcheck of container 1"
health_check_url1="http://65.2.121.61/app1/"  
http_status1=$(curl -s -o /dev/null  -w "%{http_code}" $health_check_url1)

if [ "$http_status1" = "200" ]; then
    echo "App1-container is healthy."
else
    echo "App1-container is not healthy. HTTP status code: $http_status1"
fi

echo "Healthcheck of container 2"
health_check_url2="http://65.2.121.61/app2/"  
http_status2=$(curl -s -o /dev/null  -w "%{http_code}" $health_check_url2)

if [ "$http_status2" = "200" ]; then
    echo "app2-container is healthy."
else
    echo "App2-container is not healthy. HTTP status code: $http_status2"
fi
